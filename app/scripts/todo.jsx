/** @jsx React.DOM */
var React = require('react');
var ReactDOM = require('react-dom');

var ToDo = React.createClass({
    getInitialState: function() {
        return {'text': '', task : [], color: [], visibility: [], stop: false};
    },

    makeList: function(e){
        return (
            <table>
                {this.createItems(e.value)}
            </table>
        );
    },
    createItems: function(e){
        var output = [];
        for(var i = 0; i < e.length; i++) {
            output.push(
                <tbody>
                    <tr>
                        <td width="20">
                            <button value={i} id={i+10} className="doneButton" onClick={this.Done}>Ok</button>
                        </td>
                        <td id={i} width="200">
                            <p className="task">{e[i]}</p>
                        </td>
                        <td width="20">
                            <button value={i} className="delButton" onClick={this.Delete}>Del</button>
                        </td>
                    </tr>
                </tbody>
            );
        }
        return output;
    },
    Done: function(e) {
        e.target.style.visibility = "hidden";
        document.getElementById(e.target.value).style.color = "grey";
        this.state.color[e.target.value] = "grey";
        this.state.visibility[e.target.value] = "hidden";
    },
    Delete: function(e) {
        this.state.task.splice(e.target.value, 1);
        this.state.color.splice(e.target.value, 1);
        this.state.visibility.splice(e.target.value, 1);
        this.setState({ stop: false });
        for (var i=0; i<this.state.task.length; i++) {
            document.getElementById(i).style.color = this.state.color[i];
            document.getElementById(i+10).style.visibility = this.state.visibility[i];
        }
    },

    textChange: function(e) {
        this.setState({text: e.target.value});
    },

    submitText: function(e) {
        e.preventDefault();
        if (this.state.text !== '' && this.state.task.length < 10) {
            var nextItems = this.state.task.concat([this.state.text]);
            var color = this.state.color.concat("black");
            var vis = this.state.visibility.concat("visible");
            this.setState({task: nextItems, color: color, visibility: vis, text: ''});
            if (this.state.task.length == 9){
                this.setState({stop: true});
            }
        }
    },
    formText: function() {
        if (!this.state.stop) {
            return (
                <form className="center" onSubmit={this.submitText}>
                    <input className="formText" placeholder="New Task" type="text" size="20" maxLength="14" onChange={this.textChange} value={this.state.text} />
                </form>
            );
        }
        else {
            return (
                <h5 className="center">the end of the list</h5>
            );
        }
    },

    render: function() {
        return (
            <div>
                <div className="header">
                    <p>My Task List</p>
                </div>
                <this.makeList value={this.state.task} key={this.state.task.id} />
                <this.formText />
            </div>
        );
    }
});

ReactDOM.render(
    <ToDo />,
    document.getElementById('content')
);
