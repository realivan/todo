/* Created by simatic on 16.03.16. */
module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        react: {
            files: {
                expand: true,
                cwd: 'app/scripts/',
                src: ['**/*.jsx'],
                dest: 'app/scripts/build',
                ext: '.js'
            }
        },
        concat: {
            dist: {
                src: ['app/scripts/build/todo.js'],
                dest: 'app/scripts/build/production.js'
            }
        },
        jshint: {
            foo: {
                src: ['app/scripts/build/production.js']
            }
        },
        browserify: {
            compile: {
                src: 'app/scripts/build/production.js',
                dest: 'app/scripts/build/build.js'
            }
        },
        uglify: {
            build: {
                src: 'app/scripts/build/build.js',
                dest: 'app/scripts/todo.min.js'
            }
        },
        connect: {
            server: {
                options: {
                    port: 8000,
                    hostname: '*',
                    keepalive: true
                }
            }
        },
        watch: {
            scripts: {
                files: ['app/scripts/todo.jsx'],
                tasks: ['react', 'concat', 'browserify', 'uglify'],
                options: {
                    spawn: false
                }
            },
            css: {
                files: ['app/styles/*.sass'],
                tasks: ['sass'],
                options: { spawn: false }
            }
        },
        sass: {
            dist: {
                options: { style: 'compressed' },
                files: { 'app/styles/todo.css': 'app/styles/todo.sass' }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-react');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-browserify');

    grunt.registerTask('default', ['react', 'concat', 'browserify', 'uglify']);

};

